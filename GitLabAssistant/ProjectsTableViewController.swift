//
//  ProjectsTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class ProjectsTableViewController: UITableViewController {
    
    static let cache = NSCache<NSString, UIImage>()
    
    var projects = [ProjectsElement]()
    var user: User?
    let projectListSection = 1
    var selectedProject: ProjectsElement?
    
    let api: String = "https://gitlab.com/api/v4"
    let endpoint: String = "/projects?"
    let query: String = "owned=false&membership=true&order_by=last_activity_at&sort=desc"
    
    let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
    let retrievedUsername: String? = KeychainWrapper.standard.string(forKey: "username")
    
    
    
    @IBOutlet var userBarButtonItem: UIBarButtonItem! 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        sendRequest(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
        getUser(url: "https://gitlab.com/api/v4/user", accessToken: retrievedAccessToken!)
        
        if let avatarURL = user?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.userBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: nil)
                    }
                }
            }
        } else {
            let image = UIImage(named: "User_40")
            userBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: nil)
        }
        
    }
    
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return projectListSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return projects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as! ProjectTableViewCell
        let project = projects[indexPath.row]
        cell.project = project
        
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let project = projects[indexPath.row]
        selectedProject = project
        performSegue(withIdentifier: "showProject", sender: nil)
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showProject" {
            let projectTableViewController = segue.destination as! ProjectTableViewController
            projectTableViewController.project = selectedProject
            
        }
        
    }
    // MARK: - Action
    
    @IBAction func refreshControl(_ sender: UIRefreshControl) {
        sendRequest(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
        refreshControl?.endRefreshing()
    }
    
    @IBAction func signOut(_ sender: UIBarButtonItem) {
        KeychainWrapper.standard.removeAllKeys()
        let loginTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginTableViewController") as! LoginTableViewController
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = loginTableViewController
    }
    
    func getUser(url: String, accessToken: String) {
        guard let URL = URL(string: url) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                self.user = try decoder.decode(User.self, from: data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
    }
    
    func sendRequest(_ api: String, endpoint: String, query: String, accessToken: String) {
        
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        guard let URL = URL(string: api+endpoint+query) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        // Headers
        
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        // Set up the session
        
        let session = URLSession.shared
        
        // Make the request
        
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let decoder = JSONDecoder()
                self.projects = try decoder.decode([ProjectsElement].self, from: data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
    }
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            if downloadedImage != nil {
                ProjectsTableViewController.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
        }
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = ProjectsTableViewController.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
    
}
