//
//  TextViewWithPlaceholder.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/28/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

@IBDesignable
class TextViewWithPlaceholder: UITextView {
    
    @IBInspectable var placeholder: String = "" {
        didSet{
            updatePlaceholder()
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            updatePlaceholder()
        }
    }
    
    private var originalTextColor = UIColor.black
    private var originalText: String = ""
    
    private func updatePlaceholder() {
        
        if self.text == "" || self.text == placeholder  {
            
            self.text = placeholder
            self.textColor = placeholderColor
        } else {
            self.textColor = self.originalTextColor
            self.originalText = self.text
        }
        
    }
    
    
    override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        self.text = self.originalText
        self.textColor = self.originalTextColor
        return result
    }
    override func resignFirstResponder() -> Bool {
        let result = super.resignFirstResponder()
        updatePlaceholder()
        
        return result
    }
}
