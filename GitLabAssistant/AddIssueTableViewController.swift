//
//  AddIssueTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/21/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class AddIssueTableViewController: UITableViewController, UITextViewDelegate {
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var titleTextView: TextViewWithPlaceholder!
    @IBOutlet weak var descriptionTextView: TextViewWithPlaceholder!
    @IBOutlet weak var dueDateTextField: UITextField!
    @IBOutlet weak var labelLabel: UILabel!
    @IBOutlet weak var MilestoneLabel: UILabel!
    @IBOutlet weak var avatarImageView: CircleImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var issue: Issue?
    var newIssue: Issue?
    var project: ProjectsElement?
    var selectedProject: ProjectsElement?
    var selectedMilestone: Milestone?
    var selectedMilestoneID: Int?
    var selectedLabelsName: [String]?
    var selectedMemberID: Int?
    private var datePicker: UIDatePicker?
    
    
    var userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        dueDateTextField.inputView = datePicker
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexSpace, doneButton], animated: true)
                dueDateTextField.inputAccessoryView = toolbar
        datePicker?.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        datePicker!.minimumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let tapGestureRecognizer = UIGestureRecognizer(target: self, action: #selector(tapGestureDone))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        tableView.estimatedRowHeight = 60
    }
    
    @objc func doneAction() {
        view.endEditing(true)
    }
    @objc func dateChanged() {
        getDateFromDatePicker()
    }
    @objc func tapGestureDone() {
        view.endEditing(true)
    }
    func getDateFromDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dueDateTextField.text = dateFormatter.string(from: datePicker!.date)
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    

    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }    
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showMembers" {
            let membersTableViewController = segue.destination as? MembersTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/members"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            membersTableViewController?.project = selectedProject
            membersTableViewController?.getProjectUsers(url: url, accessToken: retrievedAccessToken!)
        }
        if segue.identifier == "showMilestones" {
            let milestonesTableViewController = segue.destination as? MilestonesTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/milestones"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            milestonesTableViewController?.project = selectedProject
            milestonesTableViewController?.getProjectMilestones(url: url, accessToken: retrievedAccessToken!)
            milestonesTableViewController?.selectedMilestone = selectedMilestone
        }
        if segue.identifier == "showLabels" {
            let labelsTableViewController = segue.destination as? LabelsTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/labels"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            labelsTableViewController?.project = selectedProject
            labelsTableViewController?.getProjectLabels(url: url, accessToken: retrievedAccessToken!)
        }
     }

    
    // MARK: - Action
    
    @IBAction func cancelAddIssue(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func selectedMilestone (segue: UIStoryboardSegue) {
        let milestoneTableViewController = segue.source as! MilestonesTableViewController
        
        if let selectedMilestone = milestoneTableViewController.selectedMilestone {
           MilestoneLabel.text = selectedMilestone.title
            selectedMilestoneID = selectedMilestone.id
        }
    }
    @IBAction func selectedLabels (segue: UIStoryboardSegue) {
        let labelsTableViewController = segue.source as! LabelsTableViewController
        let selectedLabels = labelsTableViewController.selectedLabelsName
        selectedLabelsName = selectedLabels
        labelLabel.text = selectedLabels.joined(separator: ", ")
        
        
    }
    @IBAction func selectedMember (segue: UIStoryboardSegue) {
        let membersTableViewController = segue.source as! MembersTableViewController
        
        if let selectedMember = membersTableViewController.selectedMember {
            nameLabel.text = selectedMember.name
            selectedMemberID = selectedMember.id
            if let avatarURL = selectedMember.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.avatarImageView.image = image
                        }
                    }
                }
            } else {
                avatarImageView.image = UIImage(named: "User")
            }
        }
    }
    

    func createNewIssue(url: String, accessToken: String, title: String, description: String?, assigneeID: Int?, milestoneID: Int?, labels: String?, dueDate: String?) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        guard let URL = URL(string: url) else { return }
        let parameters = ["title": title, "description": description as Any, "assignee_ids": assigneeID as Any, "milestone_id": milestoneID as Any, "labels": labels as Any, "due_date": dueDate as Any] as [String : Any]
        var request = URLRequest(url: URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
        
        
    }
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            if downloadedImage != nil {
                UserTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
        }
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = UserTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
}
