//
//  IssueTableViewCell.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class IssueTableViewCell: UITableViewCell {
    
    static let cache = NSCache<NSString, UIImage>()
    
    let dateFormatterGet = DateFormatter()
    let dateFormatter = DateFormatter()
    
    typealias ImageDataHandler = ((Data) -> Void)
    
    var issue: Issue! {
        didSet {
            nameLabel.text = issue.title
            idLabel.text = "#" + String(issue.iid ?? 0)
            authorNameLabel.text = "author: " + issue.author!.username!
            let labelsArray = issue.labels
            labelsLabel.text = labelsArray?.joined(separator: ", ")
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.dateFormat = "MMM d, yyyy"
            guard let dateString = issue.createdAt else { return }
            let date: Date? = dateFormatterGet.date(from: dateString)
            createdAtLabel.text = dateFormatter.string(from: date!)
            
            if let avatarURL = issue.assignee?.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.assigneeAvatarImageView.image = image
                        }
                    }
                }
            } else {
                assigneeAvatarImageView.image = UIImage(named: "placeholder #2")
            }
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var labelsLabel: UILabel!
    @IBOutlet weak var assigneeAvatarImageView: CircleImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                IssueTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = IssueTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }

}
