//
//  EventTableViewCell.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/25/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    static let cache = NSCache<NSString, UIImage>()
    let dateFormatterGet = DateFormatter()
    let dateFormatter = DateFormatter()

    typealias ImageDataHandler = ((Data) -> Void)
    
    var event: Event! {
        didSet {
            targetTitleLabel.text = event.targetTitle ?? event.note?.body ?? event.pushData?.commitTitle
            authorNameLabel.text = event.author?.name
            targetTypeLabel.text = event.targetType ?? event.pushData?.ref
            actionNameLabel.text = event.actionName
            
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.dateFormat = "MMM d, yyyy"
            guard let dateString = event.createdAt else { return }
            let date: Date? = dateFormatterGet.date(from: dateString)
            createAtLabel.text = dateFormatter.string(from: date!)
            
            if let avatarURL = event.author?.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.authorImageView.image = image
                        }
                    }
                }
            } else {
                authorImageView.image = UIImage(named: "User")
            }
        }
    }
    
    @IBOutlet weak var authorImageView: CircleImageView!
    @IBOutlet weak var targetTitleLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var targetTypeLabel: UILabel!
    @IBOutlet weak var actionNameLabel: UILabel!
    @IBOutlet weak var createAtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                EventTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = EventTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }

}
