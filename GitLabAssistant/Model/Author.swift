//
//  Author.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/25/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Author
struct Author: Codable {
    let name: String?
    let username: String?
    let id: Int?
    let state: String?
    let avatarUrl: String?
    let webUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case username = "username"
        case id = "id"
        case state = "state"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
    }
}
