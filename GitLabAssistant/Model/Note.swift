//
//  Note.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/25/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Note
struct Note: Codable {
    let id: Int?
    let body: String?
    let attachment: String?
    let author: Author?
    let createdAt: String?
    let system: Bool?
    let noteableId: Int?
    let noteableType: String?
    let noteableIid: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case body = "body"
        case attachment = "attachment"
        case author = "author"
        case createdAt = "created_at"
        case system = "system"
        case noteableId = "noteable_id"
        case noteableType = "noteable_type"
        case noteableIid = "noteable_iid"
    }
}
