//
//  ProjectElement.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let projectElement = try ProjectElement(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.projectElementTask(with: url) { projectElement, response, error in
//     if let projectElement = projectElement {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - ProjectElement
struct ProjectsElement: Codable {
    var id: Int?
    let projectDescription: String?
    let name: String?
    let nameWithNamespace: String?
    let path: String?
    let pathWithNamespace: String?
    let createdAt: String?
    let defaultBranch: String?
    let tagList: [String]?
    let sshUrlToRepo: String?
    let httpUrlToRepo: String?
    let webUrl: String?
    let readmeUrl: String?
    let avatarUrl: String?
    let starCount: Int?
    let forksCount: Int?
    let lastActivityAt: String?
    let namespace: Namespace?
    let links: Links?
    let emptyRepo: Bool?
    let archived: Bool?
    let visibility: Visibility?
    let owner: Owner?
    let resolveOutdatedDiffDiscussions: Bool?
    let containerRegistryEnabled: Bool?
    let issuesEnabled: Bool?
    let mergeRequestsEnabled: Bool?
    let wikiEnabled: Bool?
    let jobsEnabled: Bool?
    let snippetsEnabled: Bool?
    let sharedRunnersEnabled: Bool?
    let lfsEnabled: Bool?
    let creatorId: Int?
    let importStatus: String?
    let openIssuesCount: Int?
    let publicJobs: Bool?
    let ciConfigPath: String?
    let sharedWithGroups: [SharedWithGroups]?
    let onlyAllowMergeIfPipelineSucceeds: Bool?
    let requestAccessEnabled: Bool?
    let onlyAllowMergeIfAllDiscussionsAreResolved: Bool?
    let printingMergeRequestLinkEnabled: Bool?
    let mergeMethod: String?
    let externalAuthorizationClassificationLabel: String?
    let permissions: Permissions?
    let mirror: Bool?
    let approvalsBeforeMerge: Int?
    let statistics: Statistics?
    let packagesEnabled: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case projectDescription = "description"
        case name = "name"
        case nameWithNamespace = "name_with_namespace"
        case path = "path"
        case pathWithNamespace = "path_with_namespace"
        case createdAt = "created_at"
        case defaultBranch = "default_branch"
        case tagList = "tag_list"
        case sshUrlToRepo = "ssh_url_to_repo"
        case httpUrlToRepo = "http_url_to_repo"
        case webUrl = "web_url"
        case readmeUrl = "readme_url"
        case avatarUrl = "avatar_url"
        case starCount = "star_count"
        case forksCount = "forks_count"
        case lastActivityAt = "last_activity_at"
        case namespace = "namespace"
        case links = "_links"
        case emptyRepo = "empty_repo"
        case archived = "archived"
        case visibility = "visibility"
        case owner = "owner"
        case resolveOutdatedDiffDiscussions = "resolve_outdated_diff_discussions"
        case containerRegistryEnabled = "container_registry_enabled"
        case issuesEnabled = "issues_enabled"
        case mergeRequestsEnabled = "merge_requests_enabled"
        case wikiEnabled = "wiki_enabled"
        case jobsEnabled = "jobs_enabled"
        case snippetsEnabled = "snippets_enabled"
        case sharedRunnersEnabled = "shared_runners_enabled"
        case lfsEnabled = "lfs_enabled"
        case creatorId = "creator_id"
        case importStatus = "import_status"
        case openIssuesCount = "open_issues_count"
        case publicJobs = "public_jobs"
        case ciConfigPath = "ci_config_path"
        case sharedWithGroups = "shared_with_groups"
        case onlyAllowMergeIfPipelineSucceeds = "only_allow_merge_if_pipeline_succeeds"
        case requestAccessEnabled = "request_access_enabled"
        case onlyAllowMergeIfAllDiscussionsAreResolved = "only_allow_merge_if_all_discussions_are_resolved"
        case printingMergeRequestLinkEnabled = "printing_merge_request_link_enabled"
        case mergeMethod = "merge_method"
        case externalAuthorizationClassificationLabel = "external_authorization_classification_label"
        case permissions = "permissions"
        case mirror = "mirror"
        case approvalsBeforeMerge = "approvals_before_merge"
        case statistics = "statistics"
        case packagesEnabled = "packages_enabled"
    }
}
