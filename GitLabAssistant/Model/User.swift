//
//  User.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 7/1/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - User
struct User: Codable {
    let id: Int?
    let name: String?
    let username: String?
    let state: String?
    let avatarUrl: String?
    let webUrl: String?
    let createdAt: String?
    let bio: String?
    let location: String?
    let publicEmail: String?
    let skype: String?
    let linkedin: String?
    let twitter: String?
    let websiteUrl: String?
    let organization: String?
    let lastSignInAt: String?
    let confirmedAt: String?
    let lastActivityOn: String?
    let email: String?
    let themeId: Int?
    let colorSchemeId: Int?
    let projectsLimit: Int?
    let currentSignInAt: String?
    let identities: [Identity]?
    let canCreateGroup: Bool?
    let canCreateProject: Bool?
    let twoFactorEnabled: Bool?
    let external: Bool?
    let privateProfile: Bool?
    let sharedRunnersMinutesLimit: Int?
    let extraSharedRunnersMinutesLimit: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case username = "username"
        case state = "state"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
        case createdAt = "created_at"
        case bio = "bio"
        case location = "location"
        case publicEmail = "public_email"
        case skype = "skype"
        case linkedin = "linkedin"
        case twitter = "twitter"
        case websiteUrl = "website_url"
        case organization = "organization"
        case lastSignInAt = "last_sign_in_at"
        case confirmedAt = "confirmed_at"
        case lastActivityOn = "last_activity_on"
        case email = "email"
        case themeId = "theme_id"
        case colorSchemeId = "color_scheme_id"
        case projectsLimit = "projects_limit"
        case currentSignInAt = "current_sign_in_at"
        case identities = "identities"
        case canCreateGroup = "can_create_group"
        case canCreateProject = "can_create_project"
        case twoFactorEnabled = "two_factor_enabled"
        case external = "external"
        case privateProfile = "private_profile"
        case sharedRunnersMinutesLimit = "shared_runners_minutes_limit"
        case extraSharedRunnersMinutesLimit = "extra_shared_runners_minutes_limit"
    }
}
