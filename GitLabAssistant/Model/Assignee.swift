//
//  Assignee.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Assignee
struct Assignee: Codable {
    var id: Int?
    let name: String?
    let username: String?
    let state: String?
    let avatarUrl: String?
    let webUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case username = "username"
        case state = "state"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
    }
}
