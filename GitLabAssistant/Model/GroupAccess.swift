//
//  GroupAccess.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let groupAccess = try GroupAccess(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.groupAccessTask(with: url) { groupAccess, response, error in
//     if let groupAccess = groupAccess {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - GroupAccess
struct GroupAccess: Codable {
    let accessLevel: Int?
    let notificationLevel: Int?
    
    enum CodingKeys: String, CodingKey {
        case accessLevel = "access_level"
        case notificationLevel = "notification_level"
    }
}
