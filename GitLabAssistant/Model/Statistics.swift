//
//  Statistics.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let statistics = try Statistics(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.statisticsTask(with: url) { statistics, response, error in
//     if let statistics = statistics {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - Statistics
struct Statistics: Codable {
    let commitCount: Int?
    let storageSize: Int?
    let repositorySize: Int?
    let wikiSize: Int?
    let lfsObjectsSize: Int?
    let jobArtifactsSize: Int?
    let packagesSize: Int?
    
    enum CodingKeys: String, CodingKey {
        case commitCount = "commit_count"
        case storageSize = "storage_size"
        case repositorySize = "repository_size"
        case wikiSize = "wiki_size"
        case lfsObjectsSize = "lfs_objects_size"
        case jobArtifactsSize = "job_artifacts_size"
        case packagesSize = "packages_size"
    }
}
