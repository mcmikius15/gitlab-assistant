//
//  Event.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/25/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Event
struct Event: Codable {
    let title: String?
    let projectId: Int?
    let actionName: String?
    let targetId: Int?
    let targetIid: Int?
    let targetType: String?
    let authorId: Int?
    let targetTitle: String?
    let createdAt: String?
    let note: Note?
    let author: Author?
    let authorUsername: String?
    let pushData: PushData?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case projectId = "project_id"
        case actionName = "action_name"
        case targetId = "target_id"
        case targetIid = "target_iid"
        case targetType = "target_type"
        case authorId = "author_id"
        case targetTitle = "target_title"
        case createdAt = "created_at"
        case note = "note"
        case author = "author"
        case authorUsername = "author_username"
        case pushData = "push_data"
    }
}
