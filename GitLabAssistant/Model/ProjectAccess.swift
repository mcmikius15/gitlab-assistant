//
//  ProjectAccess.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let projectAccess = try ProjectAccess(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.projectAccessTask(with: url) { projectAccess, response, error in
//     if let projectAccess = projectAccess {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - ProjectAccess
struct ProjectAccess: Codable {
    let accessLevel: Int?
    let notificationLevel: Int?
    
    enum CodingKeys: String, CodingKey {
        case accessLevel = "access_level"
        case notificationLevel = "notification_level"
    }
}
