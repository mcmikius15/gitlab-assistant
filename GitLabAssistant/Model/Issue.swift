//
//  Issue.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Issue
struct Issue: Codable {
    var id: Int?
    var iid: Int?
    let projectId: Int?
    var title: String?
    var issueDescription: String?
    let state: String?
    let createdAt: String?
    let updatedAt: String?
    let closedAt: String?
    let closedBy: ClosedBy?
    var labels: [String]?
    var milestone: Milestone?
    let assignees: [Assignee]?
    let author: Assignee?
    var assignee: Assignee?
    let userNotesCount: Int?
    let mergeRequestsCount: Int?
    let upvotes: Int?
    let downvotes: Int?
    var dueDate: String?
    let confidential: Bool?
    let discussionLocked: Bool?
    let webUrl: String?
    let timeStats: TimeStats?
    let taskCompletionStatus: TaskCompletionStatus?
    let hasTasks: Bool?
    let issueLinks: IssueLinks?
    let subscribed: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case iid = "iid"
        case projectId = "project_id"
        case title = "title"
        case issueDescription = "description"
        case state = "state"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case closedAt = "closed_at"
        case closedBy = "closed_by"
        case labels = "labels"
        case milestone = "milestone"
        case assignees = "assignees"
        case author = "author"
        case assignee = "assignee"
        case userNotesCount = "user_notes_count"
        case mergeRequestsCount = "merge_requests_count"
        case upvotes = "upvotes"
        case downvotes = "downvotes"
        case dueDate = "due_date"
        case confidential = "confidential"
        case discussionLocked = "discussion_locked"
        case webUrl = "web_url"
        case timeStats = "time_stats"
        case taskCompletionStatus = "task_completion_status"
        case hasTasks = "has_tasks"
        case issueLinks = "_links"
        case subscribed = "subscribed"
    }
}
