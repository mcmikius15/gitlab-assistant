//
//  Namespace.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let namespace = try Namespace(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.namespaceTask(with: url) { namespace, response, error in
//     if let namespace = namespace {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - Namespace
struct Namespace: Codable {
    let id: Int?
    let name: String?
    let path: String?
    let kind: String?
    let fullPath: String?
    let parentId: Int?
    let avatarUrl: String?
    let webUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case path = "path"
        case kind = "kind"
        case fullPath = "full_path"
        case parentId = "parent_id"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
    }
}
