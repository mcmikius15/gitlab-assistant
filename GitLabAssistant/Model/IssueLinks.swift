//
//  IssueLinks.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - IssueLinks
struct IssueLinks: Codable {
    let linksSelf: String?
    let notes: String?
    let awardEmoji: String?
    let project: String?
    
    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case notes = "notes"
        case awardEmoji = "award_emoji"
        case project = "project"
    }
}
