//
//  Identity.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 7/1/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Identity
struct Identity: Codable {
    let provider: String?
    let externUid: String?
    let samlProviderId: Int?
    
    enum CodingKeys: String, CodingKey {
        case provider = "provider"
        case externUid = "extern_uid"
        case samlProviderId = "saml_provider_id"
    }
}
