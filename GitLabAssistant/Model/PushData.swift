//
//  PushData.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/25/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - PushData
struct PushData: Codable {
    let commitCount: Int?
    let action: String?
    let refType: String?
    let commitFrom: String?
    let commitTo: String?
    let ref: String?
    let commitTitle: String?
    
    enum CodingKeys: String, CodingKey {
        case commitCount = "commit_count"
        case action = "action"
        case refType = "ref_type"
        case commitFrom = "commit_from"
        case commitTo = "commit_to"
        case ref = "ref"
        case commitTitle = "commit_title"
    }
}
