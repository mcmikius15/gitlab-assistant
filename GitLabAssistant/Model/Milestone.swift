//
//  Milestone.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Milestone
struct Milestone: Codable, Equatable {
    var id: Int?
    var iid: Int?
    let projectId: Int?
    var title: String?
    let milestoneDescription: String?
    let state: String?
    let createdAt: String?
    let updatedAt: String?
    let dueDate: String?
    let startDate: String?
    let webUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case iid = "iid"
        case projectId = "project_id"
        case title = "title"
        case milestoneDescription = "description"
        case state = "state"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case dueDate = "due_date"
        case startDate = "start_date"
        case webUrl = "web_url"
    }
}
