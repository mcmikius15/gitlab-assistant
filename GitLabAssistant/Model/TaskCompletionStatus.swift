//
//  TaskCompletionStatus.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - TaskCompletionStatus
struct TaskCompletionStatus: Codable {
    let count: Int?
    let completedCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case count = "count"
        case completedCount = "completed_count"
    }
}
