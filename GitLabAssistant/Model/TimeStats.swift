//
//  TimeStats.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - TimeStats
struct TimeStats: Codable {
    let timeEstimate: Int?
    let totalTimeSpent: Int?
    let humanTimeEstimate: Int?
    let humanTotalTimeSpent: Int?
    
    enum CodingKeys: String, CodingKey {
        case timeEstimate = "time_estimate"
        case totalTimeSpent = "total_time_spent"
        case humanTimeEstimate = "human_time_estimate"
        case humanTotalTimeSpent = "human_total_time_spent"
    }
}
