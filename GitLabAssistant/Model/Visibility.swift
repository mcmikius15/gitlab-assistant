//
//  Visibility.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

enum Visibility: String, Codable {
    case visibilityPrivate = "private"
    case visibilityPublic = "public"
    case visibilityInternal = "internal"
}
