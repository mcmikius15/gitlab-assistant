//
//  Links.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let links = try Links(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.linksTask(with: url) { links, response, error in
//     if let links = links {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - Links
struct Links: Codable {
    let linksSelf: String?
    let issues: String?
    let mergeRequests: String?
    let repoBranches: String?
    let labels: String?
    let events: String?
    let members: String?
    
    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case issues = "issues"
        case mergeRequests = "merge_requests"
        case repoBranches = "repo_branches"
        case labels = "labels"
        case events = "events"
        case members = "members"
    }
}
