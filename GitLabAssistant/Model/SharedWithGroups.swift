//
//  SharedWithGroups.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let sharedWithGroups = try SharedWithGroups(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.sharedWithGroupsTask(with: url) { sharedWithGroups, response, error in
//     if let sharedWithGroups = sharedWithGroups {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - SharedWithGroups
struct SharedWithGroups: Codable {
    let groupId: Int?
    let groupName: String?
    let groupFullPath: String?
    let groupAccessLevel: Int?
    
    enum CodingKeys: String, CodingKey {
        case groupId = "group_id"
        case groupName = "group_name"
        case groupFullPath = "group_full_path"
        case groupAccessLevel = "group_access_level"
    }
}
