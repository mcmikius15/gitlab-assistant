//
//  Member.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/24/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Member
struct Member: Codable, Equatable {
    let id: Int?
    let username: String?
    let name: String?
    let state: String?
    let avatarUrl: String?
    let webUrl: String?
    let expiresAt: Date?
    let accessLevel: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case username = "username"
        case name = "name"
        case state = "state"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
        case expiresAt = "expires_at"
        case accessLevel = "access_level"
    }
}
