//
//  ProjectOwner.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let owner = try Owner(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.ownerTask(with: url) { owner, response, error in
//     if let owner = owner {
//       ...
//     }
//   }
//   task.resume()

import Foundation

// MARK: - Owner
struct Owner: Codable {
    let id: Int?
    let name: String?
    let username: String?
    let state: String?
    let avatarUrl: String?
    let webUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case username = "username"
        case state = "state"
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
    }
}
