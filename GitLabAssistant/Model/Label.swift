//
//  Label.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/26/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation

// MARK: - Label
struct Label: Codable, Equatable {
    let id: Int?
    let name: String?
    let color: String?
    let textColor: String?
    let labelDescription: String?
    let openIssuesCount: Int?
    let closedIssuesCount: Int?
    let openMergeRequestsCount: Int?
    let subscribed: Bool?
    let priority: Int?
    let isProjectLabel: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case color = "color"
        case textColor = "text_color"
        case labelDescription = "description"
        case openIssuesCount = "open_issues_count"
        case closedIssuesCount = "closed_issues_count"
        case openMergeRequestsCount = "open_merge_requests_count"
        case subscribed = "subscribed"
        case priority = "priority"
        case isProjectLabel = "is_project_label"
    }
}

