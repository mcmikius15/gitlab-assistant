//
//  ProjectTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/17/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class ProjectTableViewController: UITableViewController {
    
    var project: ProjectsElement?
    var issues = [Issue]()
    var members = [Member]()
    let numberOfRowsAtSection: [Int] = [1, 1, 4]
    var selectedProject: ProjectsElement?
    static let cache = NSCache<NSString, UIImage>()
    let dateFormatterGet = DateFormatter()
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var nameWithNamespaceLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var issuesLabel: UILabel!
    @IBOutlet weak var openIssuesCountLabel: UILabel!
    @IBOutlet weak var eventsLabel: UILabel!
    @IBOutlet weak var lastActivityAtLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var tagsExamplesLabel: UILabel!
    @IBOutlet weak var projectDescriptionLabel: UILabel!
    @IBOutlet weak var usersLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        title = project?.name
        
        if let avatarURL = project?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.avatarImageView.image = image
                    }
                }
            }
        } else {
            avatarImageView.image = UIImage(named: "placeholder")
        }
        idLabel.text = String(project?.id ?? 0)
        nameWithNamespaceLabel.text = project?.nameWithNamespace
        openIssuesCountLabel.text = String(project?.openIssuesCount ?? 0)
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat = "MMM d, yyyy"
        guard let dateString = project?.lastActivityAt else { return }
        let date: Date? = dateFormatterGet.date(from: dateString)
        lastActivityAtLabel.text = dateFormatter.string(from: date!)
        let tagsArray = project?.tagList
        tagsExamplesLabel.text = tagsArray?.joined(separator: ", ")
        projectDescriptionLabel.text = project?.projectDescription
        ownerLabel.text = "Owner: " + (project?.owner?.name)!
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows: Int = 0
        
        if section < numberOfRowsAtSection.count {
            rows = numberOfRowsAtSection[section]
        }
        
        return rows
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "projectDescriptionCell", for: indexPath)
     
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    /*
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     if indexPath.row == 0 {
     performSegue(withIdentifier: "showProjectIssues", sender: self)
     } else if indexPath.row == 1 {
     //configure action when tap cell 1
     }
     
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProjectIssues" {
            let projectIssuesTableViewController = segue.destination as? ProjectIssuesTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            projectIssuesTableViewController?.project = selectedProject
            projectIssuesTableViewController?.getProjectIssues(url: url, accessToken: retrievedAccessToken!)
        }
        if segue.identifier == "showProjectUsers" {
            let projectUsersTableViewController = segue.destination as? ProjectUsersTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/members"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            projectUsersTableViewController?.project = selectedProject
            projectUsersTableViewController?.getProjectUsers(url: url, accessToken: retrievedAccessToken!)
        }
        if segue.identifier == "showEvents"{
            let projectEventsTableViewController = segue.destination as? ProjectEventsTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/events"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            selectedProject = project
            projectEventsTableViewController?.project = selectedProject
            projectEventsTableViewController?.getProjectEvents(url: url, accessToken: retrievedAccessToken!)
        }
    }
    
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                ProjectTableViewController.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = ProjectTableViewController.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
    
}
