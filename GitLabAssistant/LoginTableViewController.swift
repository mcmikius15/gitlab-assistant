//
//  LoginTableViewController.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 12.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class LoginTableViewController: UITableViewController, UITextViewDelegate {

    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:))))
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginTableViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(LoginTableViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.view.endEditing(true)
            for textField in self.view.subviews where textField is UITextField {
                textField.resignFirstResponder()
            }
        }
        sender.cancelsTouchesInView = false
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Action
    @IBAction func loginUser(_ sender: RoundedButton) {
        self.view.endEditing(true)
        guard let username = usernameTextField.text, let password = passwordTextField.text else { return }
        
        if (username.isEmpty) || (password.isEmpty) {
            
            print("Username \(String(describing: username)) or password \(String(describing: password)) is empty")
            
            showErrorAlertController(title: "Alert", message: "One of the required fields is missing")
            
            return
        }
        
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        let userData = ["grant_type": "password", "username": username, "password": password,] as [String: String]
        
        guard let url = URL(string: "https://gitlab.com/oauth/token") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: userData, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            showErrorAlertController(title: "Alert", message: "Something went wrong...")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            
            if error != nil {
                self.showErrorAlertController(title: "Alert", message: "Could not successfully perform this request. Please try again later")
                print("error = \(String(describing: error))")
                return
            }
            
            print(response!)
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    let accessToken = parseJSON["access_token"] as? String
                    let tokenType = parseJSON["token_type"] as? String
                    let refreshToken = parseJSON["refresh_token"] as? String
                    let scope = parseJSON["scope"] as? String
                    let createdAt = parseJSON["created_at"] as? Int
                    print("Access token: \(String(describing: accessToken!))")
                    
                    let saveUsername: Bool = KeychainWrapper.standard.set(username, forKey: "username")
                    let saveAccessToken: Bool = KeychainWrapper.standard.set(accessToken!, forKey: "accessToken")
                    let saveTokenType: Bool = KeychainWrapper.standard.set(tokenType!, forKey: "tokenType")
                    let saveRefreshToken: Bool = KeychainWrapper.standard.set(refreshToken!, forKey: "refreshToken")
                    let saveScope: Bool = KeychainWrapper.standard.set(scope!, forKey: "scope")
                    let saveCreatedAt: Bool = KeychainWrapper.standard.set(createdAt!, forKey: "createdAt")
                    
                    print("Is save acceess Token \(saveUsername)")
                    print("Is save acceess Token \(saveAccessToken)")
                    print("Is save acceess Token \(saveTokenType)")
                    print("Is save acceess Token \(saveRefreshToken)")
                    print("Is save acceess Token \(saveScope)")
                    print("Is save acceess Token \(saveCreatedAt)")
                    
                    if (accessToken?.isEmpty)! {
                        self.showErrorAlertController(title: "Alert", message: "Something went wrong")
                        return
                    }
                    
                    DispatchQueue.main.async {
                        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController") as! MainNavigationController
                        let appDelegate = UIApplication.shared.delegate
                        appDelegate?.window??.rootViewController = homeViewController
                    }
                    
                } else {
                    self.showErrorAlertController(title: "Alert", message: "Something went wrong")
                }
            } catch {
                self.removeActivityIndicator(activityIndicator: activityIndicator)
                self.showErrorAlertController(title: "Alert", message: "Could not successfully perform this request. Please try again later")
                print(error)
            }
        }
        task.resume()
    }
    
    // MARK: - Other methods
    
    func showErrorAlertController(title: String, message: String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                print("Ok button tapped")
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {

        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height / 2, right: 0)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {

        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
}
