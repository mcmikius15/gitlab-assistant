//
//  UserTableViewCell.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/24/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit



class UserTableViewCell: UITableViewCell {
    
    static let cache = NSCache<NSString, UIImage>()
    typealias ImageDataHandler = ((Data) -> Void)
    var memberRole: String = ""
    
    var member: Member! {
        didSet {
            usernameLabel.text = member.name
            stateLabel.text = member.state
            
            if let avatarURL = member.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.userAvatarImageView.image = image
                        }
                    }
                }
            } else {
                userAvatarImageView.image = UIImage(named: "User")
            }
        }
    }

    @IBOutlet weak var userAvatarImageView: CircleImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                UserTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = UserTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }

}
