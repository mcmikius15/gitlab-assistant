//
//  ProjectTableViewCell.swift
//  GitLabAssistant
//
//  Created by Michail Bondarenko on 13.06.2019.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {
    
    static let cache = NSCache<NSString, UIImage>()
    
    typealias ImageDataHandler = ((Data) -> Void)
    
    let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
    
    var project: ProjectsElement! {
        didSet {
            nameLabel.text = project.name
            forksCountLabel.text = String(project.forksCount ?? 0)
            starCountLabel.text = String(project.starCount ?? 0)
            openIssuesCountLabel.text = String(project.openIssuesCount ?? 0)
            nameWithNamespaceLabel.text = project.nameWithNamespace

            if let avatarURL = project.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl, accessToken: retrievedAccessToken!) { (image) in
                        DispatchQueue.main.async {
                            self.avatarImageView.image = image
                            self.avatarImageView.contentMode = .scaleAspectFill
                        }
                    }
                }
            } else {
                avatarImageView.image = UIImage(named: "placeholder")
            }
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var forksCountLabel: UILabel!
    @IBOutlet weak var openIssuesCountLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameWithNamespaceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func downloadImage(withURL url:URL, accessToken: String, completion: @escaping (_ image:UIImage?)->()) {
        var request = URLRequest(url: url)
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        print(request)
        let dataTask = URLSession.shared.dataTask(with: request) { (data, responseURL, error) in
            var downloadedImage:UIImage?

            if let data = data {
                downloadedImage = UIImage(data: data)
            }

            if downloadedImage != nil {
                ProjectTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }

            DispatchQueue.main.async {
                completion(downloadedImage)
            }


        }

        dataTask.resume()
    }

    func getImage(withURL url:URL, accessToken: String, completion: @escaping (_ image:UIImage?)->()) {
        if let image = ProjectTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, accessToken: accessToken, completion: completion)
        }
    }

}
