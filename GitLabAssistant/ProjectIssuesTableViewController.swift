//
//  ProjectIssuesTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/20/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class ProjectIssuesTableViewController: UITableViewController {
    
    var issues = [Issue]()
    private let issuesListSection = 1
    var selectedIssue: Issue?
    var project: ProjectsElement?
    var selectedProject: ProjectsElement?
    
    private var openIssues = [Issue]()
    private var closedIssues = [Issue]()
    
    var newIssue: Issue?
    
    @IBOutlet weak var stateSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let api: String = "https://gitlab.com/api/v4"
        let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
        let url: String = api + endpoint
        let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        getProjectIssues(url: url, accessToken: retrievedAccessToken!)
        tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let api: String = "https://gitlab.com/api/v4"
        let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
        let url: String = api + endpoint
        let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        getProjectIssues(url: url, accessToken: retrievedAccessToken!)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return issuesListSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var returnValue = 0
        
        switch stateSegmentedControl.selectedSegmentIndex {
        case 0:
            returnValue = openIssues.count
            break
        case 1:
            returnValue = closedIssues.count
            break
        default:
            break
        }
        
        return returnValue
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IssueTableViewCell", for: indexPath) as! IssueTableViewCell
        
        switch stateSegmentedControl.selectedSegmentIndex {
        case 0:
            cell.issue = openIssues[indexPath.row]
        case 1:
            cell.issue = closedIssues[indexPath.row]
        default:
            break
        }
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let issue = issues[indexPath.row]
        selectedIssue = issue
        selectedIssue = issues[indexPath.row]
        
        performSegue(withIdentifier: "showIssue", sender: nil)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showIssue" {
            
            let issueTableViewController = segue.destination as! IssueTableViewController
            issueTableViewController.issue = selectedIssue
            issueTableViewController.project = project
            
        }
        if segue.identifier == "showAddIssue" {
            let navigationController = segue.destination as! UINavigationController
            let addIssueTableViewController = navigationController.topViewController as! AddIssueTableViewController
            addIssueTableViewController.project = project
        }
    }
    
    // MARK: - Action
    
    @IBAction func unwindCreateNewIssue(segue: UIStoryboardSegue) {
        guard let addIssueTableViewController = segue.source as? AddIssueTableViewController else { return }
        //        newIssue = addIssueTableViewController.issue
        let api: String = "https://gitlab.com/api/v4"
        let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
        let postURL: String = api + endpoint
        let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        guard let title = addIssueTableViewController.titleTextView.text else { return }
        let description = addIssueTableViewController.descriptionTextView.text
        let memberID = addIssueTableViewController.selectedMemberID
        let milestoneID = addIssueTableViewController.selectedMilestoneID
        let labels = addIssueTableViewController.labelLabel.text
        let dueDate = addIssueTableViewController.dueDateTextField.text
        addIssueTableViewController.createNewIssue(url: postURL, accessToken: retrievedAccessToken!, title: title, description: description, assigneeID: memberID, milestoneID: milestoneID, labels: labels, dueDate: dueDate)
        getProjectIssues(url: postURL, accessToken: retrievedAccessToken!)
        tableView.reloadData()
    }
    
    @IBAction func deletedIssue(segue: UIStoryboardSegue) {
        guard let issueTableViewController = segue.source as? IssueTableViewController else { return }
        let deleteIssue = issueTableViewController.deleteIssue
        let deleteIssueID = issueTableViewController.deleteIssueID
        let api: String = "https://gitlab.com/api/v4"
        let endpoint: String = "/projects/" + String(deleteIssue?.projectId ?? 0) + "/issues/" + String(deleteIssueID ?? 0)
        let postURL: String = api + endpoint
        let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        deleteProjectIssue(url: postURL, accessToken: retrievedAccessToken!)
        let endpointUpdateTable: String = "/projects/" + String(project?.id ?? 0) + "/issues"
        let postURLUpdateTable: String = api + endpointUpdateTable
        getProjectIssues(url: postURLUpdateTable, accessToken: retrievedAccessToken!)
        tableView.reloadData()
    }
    
    @IBAction func refreshControl(_ sender: UIRefreshControl) {
        
        switch stateSegmentedControl.selectedSegmentIndex {
        case 0:
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
            let query: String = "?state=opened"
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            getFilteredProjectIssues(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
            refreshControl?.endRefreshing()
        case 1:
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
            let query: String = "?state=closed"
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            getFilteredProjectIssues(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
            refreshControl?.endRefreshing()
        default:
            break
        }
    }
    @IBAction func filterIssuesByState(_ sender: UISegmentedControl) {
        switch stateSegmentedControl.selectedSegmentIndex {
        case 0:
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
            let query: String = "?state=opened"
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            getFilteredProjectIssues(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
        case 1:
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/issues"
            let query: String = "?state=closed"
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            getFilteredProjectIssues(api, endpoint: endpoint, query: query, accessToken: retrievedAccessToken!)
        default:
            break
        }
        
    }
    @IBAction func addNewIssue(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "showAddIssue", sender: nil)
    }
    
    
    func getProjectIssues(url: String, accessToken: String) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        guard let URL = URL(string: url) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let decoder = JSONDecoder()
                self.issues = try decoder.decode([Issue].self, from: data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            } catch {
                print(error)
            }
            }.resume()
    }
    
    func deleteProjectIssue(url: String, accessToken: String) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        let url: String = url
        guard let URL = URL(string: url) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
    }
    
    func getFilteredProjectIssues(_ api: String, endpoint: String, query: String, accessToken: String) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        let url: String = api + endpoint + query
        guard let URL = URL(string: url) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let decoder = JSONDecoder()
                self.issues = try decoder.decode([Issue].self, from: data)
                print(self.issues)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            } catch {
                print(error)
            }
            }.resume()
    }
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
}
