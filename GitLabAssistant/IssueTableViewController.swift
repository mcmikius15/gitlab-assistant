//
//  IssueTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/21/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class IssueTableViewController: UITableViewController {
    
    var issue: Issue?
    var project: ProjectsElement?
    var selectedProject: ProjectsElement?
    var issueState: String?
    
    static let cache = NSCache<NSString, UIImage>()
    let dateFormatterGet = DateFormatter()
    let dateFormatter = DateFormatter()
    
    var deleteIssueID: Int? = 0
    var deleteIssue: Issue?
    
    @IBOutlet weak var issueIDLabel: UILabel!
    @IBOutlet weak var createAtLabel: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var assigneeAvatarImageView: CircleImageView!
    @IBOutlet weak var assigneeNameLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var labelsLabel: UILabel!
    @IBOutlet weak var milestoneLabel: UILabel!
    @IBOutlet weak var authorImageView: CircleImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var changeStateButton: RoundedButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        title = issue?.title
        
        if issue?.state == "opened" {
            changeStateButton.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.6392156863, blue: 0.1490196078, alpha: 1)
            changeStateButton.setTitle("Close Issue",for: .normal)
        }
        if issue?.state == "closed" {
            changeStateButton.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            changeStateButton.setTitle("Reopen Issue",for: .normal)
        }
        
        issueIDLabel.text = "# " + String(issue?.iid ?? 0)
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat = "MMM d, yyyy"
        guard let dateString = issue?.createdAt else { return }
        let date: Date? = dateFormatterGet.date(from: dateString)
        createAtLabel.text = dateFormatter.string(from: date!)
        titleTextView.text = issue?.title
        let sizeThatShouldFitTheContent = titleTextView.sizeThatFits(titleTextView.frame.size)
        let height = sizeThatShouldFitTheContent.height
        descriptionTextView.text = issue?.issueDescription
        
        if let avatarURL = issue?.assignee?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.assigneeAvatarImageView.image = image
                    }
                }
            }
        } else {
            assigneeAvatarImageView.image = UIImage(named: "User")
        }
        
        assigneeNameLabel.text = issue?.assignee?.name
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "MMM d, yyyy"
        if let dueDateString = issue?.dueDate {
            let dueDate: Date = dateFormatterGet.date(from: dueDateString)!
            dueDateLabel.text = dateFormatter.string(from: dueDate)
        } else {
            dueDateLabel.text = "No Due Date"
            dueDateLabel.textColor = UIColor.lightGray
        }
        
        
        let labelsArray = issue?.labels
        labelsLabel.text = labelsArray?.joined(separator: ", ")
        milestoneLabel.text = issue?.milestone?.title
        
        if let avatarURL = issue?.author?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.authorImageView.image = image
                    }
                }
            }
        } else {
            authorImageView.image = UIImage(named: "User")
        }
        
        authorNameLabel.text = issue?.author?.name
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return UITableView.automaticDimension
        }
        if indexPath.row == 2 {
            return UITableView.automaticDimension
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "unwindDeletedIssue" {
            deleteIssue = issue
            deleteIssueID = issue?.iid
        }
        if segue.identifier == "showEditIssue" {
            let navigationController = segue.destination as! UINavigationController
            let editIssueTableViewController = navigationController.topViewController as! EditIssueTableViewController
            editIssueTableViewController.issue = issue
            editIssueTableViewController.project = project
        }
    }
    
    
    // MARK: - IBAction
    
    @IBAction func saveIsuueChanged(segue: UIStoryboardSegue) {
        // TODO: - Update view
        guard let editIssueTableViewController = segue.source as? EditIssueTableViewController else { return }
        let changedIssue = editIssueTableViewController.issue
        let api: String = "https://gitlab.com/api/v4"
        let endpoint: String = "/projects/" + String(changedIssue?.projectId ?? 0) + "/issues/" + String(changedIssue?.iid ?? 0)
        let getURL: String = api + endpoint
        print(getURL)
        let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        fetchChangedIssue(url: getURL, accessToken: retrievedAccessToken!)
        titleTextView.text = issue?.title
        descriptionTextView.text = issue?.issueDescription
        assigneeNameLabel.text = issue?.assignee?.name
        if let avatarURL = issue?.author?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.assigneeAvatarImageView.image = image
                    }
                }
            }
        } else {
            assigneeAvatarImageView.image = UIImage(named: "User")
        }
        dueDateLabel.text = issue?.dueDate
        let labelsArray = issue?.labels
        labelsLabel.text = labelsArray?.joined(separator: ", ")
        milestoneLabel.text = issue?.milestone?.title
        authorNameLabel.text = changedIssue?.author?.name
        if let avatarURL = changedIssue?.author?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.authorImageView.image = image
                    }
                }
            }
        } else {
            authorImageView.image = UIImage(named: "User")
        }
    }
    
    @IBAction func changeIssueState(_ sender: RoundedButton) {
        if issue?.state == "opened" {
            issueState = "close"
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(issue?.projectId ?? 0) + "/issues/" + String(issue?.iid ?? 0)
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = false
            activityIndicator.startAnimating()
            view.addSubview(activityIndicator)
            guard let URL = URL(string: url) else { return }
            let parameters = ["state_event": issueState]
            var request = URLRequest(url: URL)
            request.httpMethod = "PUT"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(retrievedAccessToken!)", forHTTPHeaderField: "Authorization")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                guard let data = data else { return }
                print(data)
                self.removeActivityIndicator(activityIndicator: activityIndicator)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.changeStateButton.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                        self.changeStateButton.setTitle("Reopen Issue",for: .normal)
                    }
                } catch {
                    print(error)
                }
                }.resume()
        }
        if issue?.state == "closed" {
            issueState = "reopen"
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(issue?.projectId ?? 0) + "/issues/" + String(issue?.iid ?? 0)
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = false
            activityIndicator.startAnimating()
            view.addSubview(activityIndicator)
            guard let URL = URL(string: url) else { return }
            let parameters = ["state_event": issueState]
            var request = URLRequest(url: URL)
            request.httpMethod = "PUT"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(retrievedAccessToken!)", forHTTPHeaderField: "Authorization")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
            request.httpBody = httpBody
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                }
                guard let data = data else { return }
                self.removeActivityIndicator(activityIndicator: activityIndicator)
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.changeStateButton.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.6392156863, blue: 0.1490196078, alpha: 1)
                        self.changeStateButton.setTitle("Close Issue",for: .normal)
                    }
                } catch {
                    print(error)
                }
                }.resume()
        }
    }
    
    
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                IssueTableViewController.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = IssueTableViewController.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
    func fetchChangedIssue(url: String, accessToken: String) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        
        guard let URL = URL(string: url) else { return }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            guard let data = data else { return }
            print(data)
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let decoder = JSONDecoder()
                self.issue = try decoder.decode(Issue.self, from: data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
    }
    
    
}
