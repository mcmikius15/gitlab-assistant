//
//  ProjectIssuesTableViewController+extension.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 7/11/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import Foundation
import UIKit

extension ProjectEventsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    func filterContentForSearchText(_ searchText: String) {
        filteredEvents = events.filter({ (event) -> Bool in
            return (event.title?.lowercased().contains(searchText.lowercased()))!
        })
        tableView.reloadData()
    }
}
