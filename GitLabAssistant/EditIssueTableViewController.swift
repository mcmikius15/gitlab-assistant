//
//  EditIssueTableViewController.swift
//  GitLabAssistant
//
//  Created by Mykhailo Bondarenko on 6/28/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class EditIssueTableViewController: UITableViewController, UITextViewDelegate {

    var issue: Issue?
    var project: ProjectsElement?
    var milestone: Milestone?
    var assignee: Member?
    var labels: [String]?
    
    let dateFormatterGet = DateFormatter()
    let dateFormatter = DateFormatter()
    
    private var datePicker: UIDatePicker?
    
    var userDefaults = UserDefaults.standard
    
    @IBOutlet weak var issueIDLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var issueTitleTextView: UITextView!
    @IBOutlet weak var issueDescriptionTextView: UITextView!
    @IBOutlet weak var assigneeImageView: CircleImageView!
    @IBOutlet weak var assigneeNameLabel: UILabel!
    @IBOutlet weak var dueDateTextField: UITextField!
    @IBOutlet weak var labelsLabel: UILabel!
    @IBOutlet weak var milestoneLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        title = issue?.title
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        dueDateTextField.inputView = datePicker
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexSpace, doneButton], animated: true)
        dueDateTextField.inputAccessoryView = toolbar
        datePicker?.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        datePicker!.minimumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let tapGestureRecognizer = UIGestureRecognizer(target: self, action: #selector(tapGestureDone))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat = "MMM d, yyyy"
        guard let dateString = issue?.createdAt else { return }
        let date: Date? = dateFormatterGet.date(from: dateString)
        createdAtLabel.text = dateFormatter.string(from: date!)
        issueIDLabel.text = "# " + String(issue?.iid ?? 0)
        issueTitleTextView.text = issue?.title
        issueDescriptionTextView.text = issue?.issueDescription
        if let avatarURL = issue?.assignee?.avatarUrl {
            if let imageUrl = URL(string: avatarURL) {
                getImage(withURL: imageUrl) { (image) in
                    DispatchQueue.main.async {
                        self.assigneeImageView.image = image
                    }
                }
            }
        } else {
            assigneeImageView.image = UIImage(named: "User")
        }
        assigneeNameLabel.text = issue?.assignee?.name
        dueDateTextField.text = issue?.dueDate
        let labelsArray = issue?.labels
        labelsLabel.text = labelsArray?.joined(separator: ", ")
        milestoneLabel.text = issue?.milestone?.title
        
        tableView.estimatedRowHeight = 60
//        tableView.rowHeight = UITableView.automaticDimension
    }
    
    @objc func doneAction() {
        view.endEditing(true)
    }
    @objc func dateChanged() {
        getDateFromDatePicker()
    }
    @objc func tapGestureDone() {
        view.endEditing(true)
    }
    func getDateFromDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dueDateTextField.text = dateFormatter.string(from: datePicker!.date)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 1 && indexPath.row == 2 {
//            return UITableView.automaticDimension
//        }
//        return super.tableView(tableView, heightForRowAt: indexPath)
        return UITableView.automaticDimension
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showEditAssegnee" {
            let changeAssigneeTableViewController = segue.destination as? ChangeAssigneeTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/members"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            changeAssigneeTableViewController?.getProjectUsers(url: url, accessToken: retrievedAccessToken!)
            changeAssigneeTableViewController?.selectedAssignee = assignee
        }
        if segue.identifier == "showEditMilestone" {
            let changeMilestoneTableViewController = segue.destination as? ChangeMilestoneTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/milestones"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            changeMilestoneTableViewController?.getProjectMilestones(url: url, accessToken: retrievedAccessToken!)
            changeMilestoneTableViewController?.selectedMilestone = milestone
        }
        if segue.identifier == "showEditLabels" {
            let changeLabelsTableViewController = segue.destination as? ChangeLabelsTableViewController
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(project?.id ?? 0) + "/labels"
            let url: String = api + endpoint
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            changeLabelsTableViewController?.getProjectLabels(url: url, accessToken: retrievedAccessToken!)
        }
        if segue.identifier == "unwindSaveChangedIssue" {
            guard let title = issueTitleTextView.text else { return }
            let description = issueDescriptionTextView.text
            let assigneeID = assignee?.id
            let dueDate = dueDateTextField.text
            let labels = labelsLabel.text
            let milestoneID = milestone?.id
            let api: String = "https://gitlab.com/api/v4"
            let endpoint: String = "/projects/" + String(issue?.projectId ?? 0) + "/issues/" + String(issue?.iid ?? 0)
            let postURL: String = api + endpoint
            print(postURL)
            let retrievedAccessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
            editIssue(url: postURL, accessToken: retrievedAccessToken!, title: title, description: description, assigneeID: assigneeID, milestoneID: milestoneID, labels: labels, dueDate: dueDate)
        }
    }

    
    // MARK: - IBActions
    
    @IBAction func cancelEditIssue(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changedMilestone (segue: UIStoryboardSegue) {
        let changeMilestoneTableViewController = segue.source as! ChangeMilestoneTableViewController
        
        if let selectedMilestone = changeMilestoneTableViewController.selectedMilestone {
            milestoneLabel.text = selectedMilestone.title
            milestone = selectedMilestone
        }
    }
    
    
    
    @IBAction func changedAssegnee (segue: UIStoryboardSegue) {
        let changeAssigneeTableViewController = segue.source as! ChangeAssigneeTableViewController
        if let selectedAssignee = changeAssigneeTableViewController.selectedAssignee {
            assignee = selectedAssignee
            assigneeNameLabel.text = selectedAssignee.name
            if let avatarURL = selectedAssignee.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.assigneeImageView.image = image
                        }
                    }
                }
            } else {
                assigneeImageView.image = UIImage(named: "User")
            }
        } else {
            assigneeNameLabel.text = issue?.assignee?.name
            if let avatarURL = issue?.assignee?.avatarUrl {
                if let imageUrl = URL(string: avatarURL) {
                    getImage(withURL: imageUrl) { (image) in
                        DispatchQueue.main.async {
                            self.assigneeImageView.image = image
                        }
                    }
                }
            } else {
                assigneeImageView.image = UIImage(named: "User")
            }
        }
    }
    @IBAction func changedLabels (segue: UIStoryboardSegue) {
        let changeLabelsTableViewController = segue.source as! ChangeLabelsTableViewController
        let selectedLabels = changeLabelsTableViewController.selectedLabelsName
        labels = selectedLabels
        labelsLabel.text = selectedLabels.joined(separator: ", ")
    }
    
    func editIssue(url: String, accessToken: String, title: String, description: String?, assigneeID: Int?, milestoneID: Int?, labels: String?, dueDate: String?) {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        guard let URL = URL(string: url) else { return }
        let parameters = ["title": title, "description": description as Any, "assignee_ids": assigneeID as Any, "milestone_id": milestoneID as Any, "labels": labels as Any, "due_date": dueDate as Any] as [String : Any]
        var request = URLRequest(url: URL)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if response != nil {
            }
            guard let data = data else { return }
            self.removeActivityIndicator(activityIndicator: activityIndicator)
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print(error)
            }
            }.resume()
        
        
    }
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
    func downloadImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            var downloadedImage:UIImage?
            
            if let data = data {
                downloadedImage = UIImage(data: data)
            }
            
            if downloadedImage != nil {
                ProjectTableViewCell.cache.setObject(downloadedImage!, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async {
                completion(downloadedImage)
            }
            
        }
        
        dataTask.resume()
    }
    
    func getImage(withURL url:URL, completion: @escaping (_ image:UIImage?)->()) {
        if let image = ProjectTableViewCell.cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }

}
